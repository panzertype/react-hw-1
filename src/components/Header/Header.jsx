import React from 'react';
import Logo from '../Header/components/Logo/Logo';
import Button from '../../common/Button/Button';

export default function Header() {
	return (
		<nav className='border border-danger border-2 navbar px-4 py-1'>
			<Logo />
			<ul className='navbar-nav flex-row gap-3 ms-auto'>
				<li className='align-self-center'>Dave</li>
				<li>
					<Button title='Logout' />
				</li>
			</ul>
		</nav>
	);
}
