import React, { useState } from 'react';
import './styles/App.css';
import Header from './components/Header/Header';
import Courses from './components/Courses/Courses';
import CreateCourse from './components/CreateCourse/CreateCourse';
import getTime from './helpers/pipeDuration';

function App() {
	const [showCreateCourseMenu, setShowCreateCourseMenu] = useState(false);

	return (
		<div className='container'>
			<div className='App'>
				<Header />
				{showCreateCourseMenu ? (
					<CreateCourse
						getTime={getTime}
						onCreate={() => setShowCreateCourseMenu(!showCreateCourseMenu)}
					/>
				) : (
					<Courses
						getTime={getTime}
						onCreate={() => setShowCreateCourseMenu(!showCreateCourseMenu)}
					/>
				)}
			</div>
		</div>
	);
}

export default App;
